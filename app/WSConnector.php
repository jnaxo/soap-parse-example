<?php

namespace App;

class WSConnector
{

    private $xmlRequest;

    function __construct()
    {
        $xml = new SimpleXMLElement('<NormalXmlGoesHere xmlns="https://api.xyz.com/DataService/"></NormalXmlGoesHere>');
        $xml->addChild('Data', 'Test');
        $customXML = new SimpleXMLElement($xml->asXML());
        $dom = dom_import_simplexml($customXML);
        $cleanXml = $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement);

        $soapHeader = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body>';
        $soapFooter = '</soapenv:Body></soapenv:Envelope>';
        $xmlRequest = $soapheader . $cleanXml . $soapFooter;
    }

}
